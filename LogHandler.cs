﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

public class LogHandler
{
    static Dictionary<string, StreamWriter> tagLogWriters = new Dictionary<string, StreamWriter>();
    static string singleUsedLogTag = ""; //Empty if many!

    public static void Initialize(string inLogTag, bool isSecondaryLog = false) {
        var randID = new Random().Next(100000, 999999);
        var fileName = inLogTag + "_log_" + randID + ".txt";

        string folderPath = @"C:\loghandlerClients\"+inLogTag;

        var fullPath = Path.Combine(folderPath, fileName);

        var fileInf = new FileInfo(fullPath);
        Directory.CreateDirectory(fileInf.DirectoryName);

        tagLogWriters.Add(inLogTag, new StreamWriter(fileInf.FullName));

        //fileInf.OpenWrite()

        if (singleUsedLogTag == "") {
            singleUsedLogTag = inLogTag;
        }
        else if (!isSecondaryLog) {
            singleUsedLogTag = ""; //empty this in this situation as there is many!
        }

        LogLine("Starting "+ inLogTag + " LogHandler with logging to file" + fileName + " !");
    }

    public static void LogLine() {
        LogLine("", true/*, logOfTag*/);
    }

    public static void LogLine(string v, bool logToConsoleToo = true, string logOfTag = "") {
        var formatted = string.Format("[{0}]{1}", System.DateTime.Now.ToString(), v);
        Log(formatted + System.Environment.NewLine, logToConsoleToo, logOfTag);
    }

    private static void Log(string v, bool logToConsoleToo = true, string logOfTag = "") {
        if (logToConsoleToo) {
            Console.Write(v);
        }

        StreamWriter logWriter;
        if(logOfTag == "") {
            logWriter = tagLogWriters[singleUsedLogTag];
        }
        else {
            logWriter = tagLogWriters[logOfTag];
        }

        //should be super safe and super slow?
        logWriter.Write(v);
        logWriter.Flush();

        //logWriter.WriteAsync(v); //faster, unreliablier?
    }
}
