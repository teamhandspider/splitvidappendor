﻿using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;

namespace splitvidappendor
{
    class Program
    {
        public static string MainPath
        {
            get
            {
                var exepath = Process.GetCurrentProcess().MainModule.FileName;

                var iteratingpath = new FileInfo(exepath).Directory;
                while (true)
                {
                    if (iteratingpath.Name == "splitvidappendor")
                        return iteratingpath.FullName;

                    iteratingpath = iteratingpath.Parent;
                }
            }
        }

        public static string FFProbePath { 
            get
            {                    
               return Path.Combine(MainPath, "ffprobe.exe");
            }
        }
        public static string FFMPEGPath
        {
            get
            {
                return Path.Combine(MainPath, "ffmpeg.exe");
            }
        }

        static void Main(string[] args)
        {
            //args = new[] { @"D:\eachine\video\20191224140047.mp4" };
            //args = new[] { @"D:\appendemo\20191224140448.mp4" };            

            //args = new[] { @"H:\fimi\autoapp\VID_20200112_145138_0025.MP4" };                        

            //args = new[] { @"E:\piilocam\VIDEO\MOVI0071.avi" };          

            //args = new[] { @"C:\Users\Max\AppData\LocalLow\ForbiddenStudios\Among the Trolls\BGGameVideoCapture\goodtest\r132527717104561367_APUSTAJA-PC_Max_segm0.mp4" };          

            //args = new[] { @"E:\dropboxes\maxteam Dropbox\Max Lindblad\Apps\bgvideo\132542599173877555_DESKTOP-3S9J2NH_veli__BUILD_cs1515\132542599173877555_DESKTOP-3S9J2NH_veli__BUILD_cs1515_segm0.mp4" };          
            //args = new[] { @"E:\dropboxes\maxteam Dropbox\Max Lindblad\Apps\bgvideo\132544112902053638_DESKTOP-3S9J2NH_veli__BUILD_cs1515\132544112902053638_DESKTOP-3S9J2NH_veli__BUILD_cs1515_segm0.mp4" };

            //args = new[] { @"E:\dropboxes\maxteam Dropbox\Max Lindblad\Apps\bgvideo\132970166593872501_LAPTOP-4DLJSJDS_Tinny__BUILD_cs7115\132970166593872501_LAPTOP-4DLJSJDS_Tinny__BUILD_cs7115_segm0.mp4" };

            //args = new[] { @"D:\bork\133011591194778178_01L32339_jsingh__BUILD_cs7825" };

            


            var safe = true;
            //safe = false;

            if(safe) {
                try {
                    Run(args);
                }
                catch (System.Exception e) {
                    LogError("Failed running! Error:\n" + e);
                    Log("");
                    Log("Press any key to exit.");
                    Console.ReadKey();

                    throw e;
                }
            }
            else {
                Run(args);
            }

        }


        static string extension = "NOTST";

        static long chainedChunksSize;

        private static void Run(string[] args) {
            LogHandler.Initialize("splitvidappendor");

            if(args.Any(x => x == "register")) {
                RegisterToMenu();
                return;
            }


            if (args.Length == 0) {
                Console.WriteLine("No arguments given! To use, please give exactly one argument, that being the starting segment videofile path.");
                Log("Press any key to exit.");
                Console.ReadKey();
                return;
            }

            var inPath = args[0];
            bool isFolderArg = false;

            if (Directory.Exists(inPath)) {
                isFolderArg = true;
                Console.WriteLine("Is a folder, trying to get _segm0 file from there");
                inPath = new DirectoryInfo(inPath).GetFiles("*").First(x => x.Name.Contains("segm0")).FullName;
                Console.WriteLine("Got: "+inPath);
            }

            extension = new FileInfo(inPath).Extension;
            Console.WriteLine("File extension is " + extension);

            var infos = new FileInfo(inPath).Directory.GetFiles($"*{extension}");

            var startTimes = new Dictionary<FileInfo, System.DateTime>();
            foreach (var item in infos) {
                //Console.WriteLine("Adding " + item);
                if (item.Name.Contains("autoappend")) continue;
                startTimes.Add(item, GetTimeFromFile(item));
            }
            Log(startTimes.Count + " neighbors");

            var goodChunks = new List<FileInfo>();

                       

            if (!infos.Any(x => x.FullName == inPath)) {
                LogError("main video not in all vids, wtf? did you give the path with extra slashes or something");
                Console.ReadKey();
            }

            var currFindingNextChunkFor = new FileInfo(inPath);
            goodChunks.Add(currFindingNextChunkFor);

            var chunkLen = GetVideoLen(currFindingNextChunkFor);

            while (true) {
                var foundNext = TryFindNextChunkFor(currFindingNextChunkFor, startTimes, chunkLen);
                if (foundNext == null) break;

                currFindingNextChunkFor = foundNext;
                goodChunks.Add(foundNext);
            }
            if (goodChunks.Count > 1) {
                Log("chain of " + goodChunks.Count);
                Log("chunks:\n\n" + goodChunks.Aggregate("\n", (x, y) => x + "\n" + y) + "\n\n");
            }
            else {
                LogError("No chain detected!");
                Log("Press any key to exit.");
                Console.ReadKey();
                return;
            }

            chainedChunksSize = goodChunks.Sum(x => x.Length);
            Log("Size of neighbors totalled:" + ByteLenghtToHumanReadable(chainedChunksSize));

            AppendClips(goodChunks, out var endResultPath);

            Log("Merging done, attempted to chain " + goodChunks.Count + " segments together.");

            //Thread.Sleep(2000); //randomly wait x time and hope the files are released?
            var moveAll = true;
            if(inPath.Contains("_cs") || inPath.Contains("_EDITOR")) {
                moveAll = false;
            }
            if(moveAll) {
                MoveOldPartsToAnotherFolder(inPath, goodChunks);
            }

            if (isFolderArg) {
                Console.WriteLine("Was folder arg, moving file to parent folder");
                var moveToPath = Path.Combine(new FileInfo(endResultFileName).Directory.Parent.FullName, new FileInfo(endResultFileName).Name);
                Console.WriteLine("copyToPath:" + moveToPath);
                File.Move(endResultFileName, moveToPath);
            }

            Log("All done!");

            Log("");
            Log("Press any key to exit.");
            Console.ReadKey();
        }

        private const string MenuName = "Folder\\shell\\NewMenuOption";
        private const string Command = "Folder\\shell\\NewMenuOption\\command";

        private static void RegisterToMenu()
        {
            Console.WriteLine("Attempting to register to context menu.");
            var command = "SplitVidAppendor";
            var exePath = System.Diagnostics.Process.GetCurrentProcess().MainModule.FileName;

            RegistryKey regmenu = null;
            RegistryKey regcmd = null;
            try {
                regmenu = Registry.ClassesRoot.CreateSubKey(MenuName);
                if (regmenu != null)
                    regmenu.SetValue("", command);
                regcmd = Registry.ClassesRoot.CreateSubKey(Command);
                if (regcmd != null)
                    regcmd.SetValue("", exePath + " %1");
            }            
            finally {
                if (regmenu != null)
                    regmenu.Close();
                if (regcmd != null)
                    regcmd.Close();
            }


            //REMOVES
            /*try {
                RegistryKey reg = Registry.ClassesRoot.OpenSubKey(Command);
                if (reg != null) {
                    reg.Close();
                    Registry.ClassesRoot.DeleteSubKey(Command);
                }
                reg = Registry.ClassesRoot.OpenSubKey(MenuName);
                if (reg != null) {
                    reg.Close();
                    Registry.ClassesRoot.DeleteSubKey(MenuName);
                }
            }
            catch (Exception ex) {
                MessageBox.Show(this, ex.ToString());
            }*/
        }

        private static void MoveOldPartsToAnotherFolder(string inPath, List<FileInfo> goodChunks) {
            var oldChaninedFolder = new DirectoryInfo(Path.Combine(new FileInfo(inPath).DirectoryName, "PartialFilesBeforeMerging"));
            oldChaninedFolder.Create();
            foreach (var item in goodChunks) {
                Log("Moving " + item.Name + " to the " + oldChaninedFolder.Name + " folder (inside the original folder)");
                var newPath = Path.Combine(oldChaninedFolder.FullName, item.Name);
                item.MoveTo(newPath);         
            }
        }

        static string endResultFileName = "$NOTSET";
        static bool ffmpegRunning = false;

        private static void AppendClips(List<FileInfo> goodChunks, out string resultFilePath)
        {
            var sb = new StringBuilder();
            foreach (var item in goodChunks)
            {
                sb.AppendLine("file '"+item.FullName+"'");
            }

            var tempFilePath = Path.Combine(MainPath, "tempclips.txt");
            File.WriteAllText(tempFilePath, sb.ToString());

            endResultFileName = goodChunks.First().FullName.Replace(extension, "") + "_autoappend" + extension;
            resultFilePath = endResultFileName;

            var args = " -f concat -safe 0 -i " + "\"" + tempFilePath + "\"";
            args += " -c copy \""+ endResultFileName+"\"";

            new Thread( () => StartMonitoringEndFile() ).Start();

            ffmpegRunning = true;
            RunCommandGetOutput(FFMPEGPath, args);
            ffmpegRunning = false;


            Thread.Sleep(1000);

            //fudge creation time to first file as this file represents it (among others)
            //var finfo = new FileInfo(endResultFileName);
            //finfo.CreationTime = goodChunks.First().CreationTime;
            File.SetCreationTime(endResultFileName, goodChunks.First().CreationTime);
        }

        private static void StartMonitoringEndFile() {
            Thread.Sleep(100);

            float bytesPerSecLerped = 0;
            float waitTime = 2f;

            long prevSize = 0;

            var handle = new FileInfo(endResultFileName);
            Log("Starting monitoring merging process. End result file path:\n\t"+endResultFileName);
            while(ffmpegRunning) {
                handle.Refresh();
                if(handle.Exists) {
                    var siz = handle.Length;
                    var prog = (float)siz / (float)chainedChunksSize;

                    var delta = siz - prevSize;
                    var bytesPerSec = delta / waitTime;
                    bytesPerSecLerped = Lerp(bytesPerSecLerped, bytesPerSec, 0.2f);

                    var bytesPerSecStr = $"{ByteLenghtToHumanReadable((long)bytesPerSecLerped)}/sec";

                    var sizRemain = chainedChunksSize - siz;
                    var timeRemain = TimeSpan.FromSeconds(sizRemain / bytesPerSecLerped);

                    var etaStr = $"ETA:{timeRemain}";

                    var progBar = GetASCIIProgressBar(prog, 50);
                    Log($"{progBar} Written {ByteLenghtToHumanReadable(siz)} of estimated full size {ByteLenghtToHumanReadable(chainedChunksSize)} {bytesPerSecStr},\t {etaStr}");

                    prevSize = siz;
                }
                else {
                    Log("File does not seem to exist (yet?)!");
                }
                Thread.Sleep((int)(waitTime * 1000));
            }
        }

        static float Lerp(float firstFloat, float secondFloat, float by) {
            return firstFloat * (1 - by) + secondFloat * by;
        }


        public static string GetASCIIProgressBar(float progressGues, int widthInChars) {
            var charsCount = widthInChars;
            //charsCount = 20;

            var donesCount = (int)System.Math.Round(progressGues * charsCount);
            var emptysCount = charsCount - donesCount;
            if (emptysCount < 0) emptysCount = 0;

            return "[" + new string('0', donesCount) + new string('_', emptysCount) + "]";

            //var lines = new String('-', Console.WindowWidth - 10);
        }

        private static FileInfo TryFindNextChunkFor(FileInfo currFindingNextChunkFor, Dictionary<FileInfo, System.DateTime> startTimes, TimeSpan chunklen)
        {
            var startTime = GetTimeFromFile(currFindingNextChunkFor);
            //var clipLen = GetVideoLen(currFindingNextChunkFor);

            var expectStartNextTime = startTime + chunklen;


            double allowBackwardsSeconds = 2;

            allowBackwardsSeconds = Math.Min(allowBackwardsSeconds, chunklen.TotalSeconds - 0.1);

            var backLine = expectStartNextTime - TimeSpan.FromSeconds(allowBackwardsSeconds);
            //var smallestDiff = startTimes.OrderBy(x => Math.Abs((x.Value - expectStartNextTime).TotalSeconds)).First();
            var validTimes = startTimes.Where(k => k.Value > backLine).ToList();
            if (validTimes.Count == 0) {
                Log("end of line (no valid options left) with " + currFindingNextChunkFor.Name);
                return null;
            }
            var smallestDiff = validTimes.OrderBy(x => Math.Abs((x.Value - expectStartNextTime).TotalSeconds)).First();

            var smallestDiffVal = smallestDiff.Value - expectStartNextTime;
            Log("smallestDiff:" + smallestDiffVal.TotalSeconds + " secs for " + smallestDiff.Key.Name);


            var allowDiffSeconds = 30f;
            if(currFindingNextChunkFor.FullName.Contains("_cs")) {
                allowDiffSeconds = 30f; //parts missing so w/e
                //allowBackwardsSeconds = 8f;
            }

            if (smallestDiffVal.TotalSeconds > -allowBackwardsSeconds && smallestDiffVal.TotalSeconds < allowDiffSeconds)
            {
                Log("Found next clip for " + currFindingNextChunkFor.Name + " : " + smallestDiff.Key.Name);
                return smallestDiff.Key;
            }
            else
            {
                Log("end of line with "+currFindingNextChunkFor.Name);
                return null;
            }
        }        

        private static TimeSpan GetVideoLen(FileInfo inClip)
        {
            var durout = RunCommandGetOutput(FFProbePath, "-v error -show_entries format=duration -of default=noprint_wrappers=1:nokey=1 " + "\"" + inClip.FullName + "\"");
            var propNumberString = durout.Split('\r').First().Replace('.', ',');
            var secs = float.Parse(propNumberString);
            var durAsSpan = System.TimeSpan.FromSeconds(secs);

            Log("GetVideoLen " + inClip.Name + ":" + durAsSpan);
            return durAsSpan;
        }

        private static DateTime GetTimeFromFile(FileInfo currFindingNextChunkFor)
        {
            DateTime parsedDateTime;
            var noExt = currFindingNextChunkFor.Name.Replace(extension, "");

            if(DateTime.TryParseExact(noExt,"yyyyMMddHHmmss", CultureInfo.InvariantCulture, DateTimeStyles.None, out parsedDateTime))
                return parsedDateTime;
            else if(noExt.StartsWith("VID_")) {
                var noVid = noExt.Replace("VID_", "");
                var lastPart = noVid.Split('_').Last();
                var noLastPart = noVid.Replace("_"+lastPart, "");
                if (DateTime.TryParseExact(noLastPart, "yyyyMMdd_HHmmss", CultureInfo.InvariantCulture, DateTimeStyles.None, out parsedDateTime))
                    return parsedDateTime;
            }
            else if(noExt.StartsWith("MOV")) {
                return currFindingNextChunkFor.CreationTime;
            }
            else if(noExt.Contains("_segm")) {
                //fucking yolo
                var afterSegm = noExt.Split("_segm").Last();
                var num = int.Parse(afterSegm);
                //var baseTime = System.DateTime.MinValue;
                var baseTime = new System.DateTime(2000, 1, 1); //don't initialize to MinValue because in weird cases this needs to go to earlier than "zero"
                var thisFakeTime = baseTime + new TimeSpan(0, 0, num * 10);
                return thisFakeTime;
            }
            else if (noExt.Contains("ASD_")) {
                //fucking yolo
                var afterAsd = noExt.Split("ASD_").Last();
                var num = int.Parse(afterAsd);
                //var baseTime = System.DateTime.MinValue;
                var baseTime = new System.DateTime(2000, 1, 1); //don't initialize to MinValue because in weird cases this needs to go to earlier than "zero"
                var thisFakeTime = baseTime + new TimeSpan(0, 0, num * 10);
                return thisFakeTime;
            }
            else {
                return currFindingNextChunkFor.LastWriteTime; //gets weird for differing use cases idk
            }

            return DateTime.MinValue;
        }

        static string RunCommandGetOutput(string exePath, string args)
        {

            Log("RunCommandGetOutput " + new FileInfo(exePath).Name + " " + args);

            Process p = new Process();
            // Redirect the output stream of the child process.
            p.StartInfo.UseShellExecute = false;
            p.StartInfo.RedirectStandardOutput = true;
            p.StartInfo.RedirectStandardError = true;
            p.StartInfo.FileName = exePath;
            p.StartInfo.Arguments = args;
            p.StartInfo.CreateNoWindow = true;

            p.Start();

            var errOutput = p.StandardError.ReadToEnd();
            if (errOutput != "")
            {
                //LogError(errOutput);
                Log("ffmpeg output:\n" + errOutput);
                return "";
            }
            else
            {
                var output = p.StandardOutput.ReadToEnd();
                p.WaitForExit();
                Log("out:" + output);
                return output;
            }
        }

        private static void LogError(string errOutput)
        {
            //Console.WriteLine("ERROR:"+ errOutput);            
            LogHandler.LogLine("ERROR:" + errOutput);
        }

        private static void Log(string v)
        {
            //Console.WriteLine(v);
            LogHandler.LogLine(v);
        }

        public static string ByteLenghtToHumanReadable(long byteLenght, bool useBits = false) {
            string suffix = "";
            long lenght;
            if (useBits) lenght = byteLenght * 8;
            else lenght = byteLenght;

            if (lenght < 1024) {
                if (useBits) suffix = " bits";
                else suffix = " B";
                return lenght + suffix;
            }
            else if (lenght < 1048576) {
                if (useBits) suffix = " kbits";
                else suffix = " kB";
                return (lenght / 1024) + suffix;
            }
            else {
                if (useBits) suffix = " Mbits";
                else suffix = " MB";
                return System.Math.Round(((float)lenght / (float)1048576), 2) + suffix;
            }
        }
    }
}
